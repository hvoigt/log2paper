package main

import (
	"fmt"
	"html/template"
	"log"
	"os"
	"strconv"

	"github.com/jasonhancock/go-cabrillo"
)

const qsoPerPage = 25

var qsoOffset = int(0)

func panicErr(err error, format string, v ...interface{}) {
	if err == nil {
		return
	}

	message := fmt.Sprintf(format, v...)
	log.Panicf("%s: %s", message, err)
}

type QSOf struct {
	Call    string
	Date    string
	Start   string
	End     string
	Mode    string
	Mhz     string
	Khz     string
	RxRst   string
	TxRst   string
	Comment string
}

type Page struct {
	Qsos []QSOf
}

func appendQSO(qso cabrillo.QSO, formattedQSOs []QSOf) []QSOf {

	t := qso.Timestamp

	year := fmt.Sprintf("%02d", t.Year())
	split := len(qso.Frequency) - 3

	return append(formattedQSOs, QSOf{
		Call:    qso.RxInfo.Callsign,
		Date:    fmt.Sprintf("%s-%02d-%02d", year[2:], t.Month(), t.Day()),
		Mode:    qso.Mode,
		Start:   fmt.Sprintf("%02d-%02d", t.Hour(), t.Minute()),
		End:     "",
		Mhz:     qso.Frequency[:split],
		Khz:     qso.Frequency[split:],
		RxRst:   qso.RxInfo.SignalReport.String(),
		TxRst:   qso.TxInfo.SignalReport.String(),
		Comment: fmt.Sprintf("Exchange Snd: %s Rcvd: %s", qso.TxInfo.Exchange, qso.RxInfo.Exchange),
	})
}

func appendQSOPage(qso cabrillo.QSO, pages []Page) []Page {

	currentPage := Page{}
	currentIndex := -1
	if len(pages) > 0 {
		currentIndex = len(pages) - 1
		currentPage = pages[currentIndex]
	}

	lenCurrentPage := len(currentPage.Qsos)
	if len(pages) <= 1 {
		lenCurrentPage = qsoOffset + lenCurrentPage
	}

	if lenCurrentPage < qsoPerPage {
		currentPage.Qsos = appendQSO(qso, currentPage.Qsos)
	} else {
		currentPage = Page{}
		currentPage.Qsos = appendQSO(qso, currentPage.Qsos)
		currentIndex = -1
	}

	if currentIndex < 0 {
		return append(pages, currentPage)
	}

	pages[currentIndex] = currentPage
	return pages
}

func main() {
	if len(os.Args) < 2 || len(os.Args) > 3 {
		log.Panicf("Usage: %s <filename> [offset]", os.Args[0])
	}
	filename := os.Args[1]

	if len(os.Args) > 2 {
		var err error
		qsoOffset, err = strconv.Atoi(os.Args[2])
		panicErr(err, "Usage: %s <filename> [offset]", os.Args[0])
	}

	f, err := os.Open(filename)
	panicErr(err, "Could not open %s", filename)
	defer f.Close()

	l, err := cabrillo.ParseLog(f)
	panicErr(err, "Could not parse %s", filename)

	formattedQSOPages := []Page{}
	for _, qso := range l.QSOs {
		formattedQSOPages = appendQSOPage(qso, formattedQSOPages)
	}
	for _, qso := range l.XQSOs {
		formattedQSOPages = appendQSOPage(qso, formattedQSOPages)
	}

	templateData, err := os.ReadFile("./template.tex")
	panicErr(err, "Could not read ./template.tex")

	t, err := template.New("tex").Parse(string(templateData))
	panicErr(err, "Could not parse template")

	err = t.Execute(os.Stdout, formattedQSOPages)
	panicErr(err, "Failed to execute template")
}
