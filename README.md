# Log 2 Paper

Writing down all contacts from a contest is cumbersome when you
normally maintain a paper log.

This tool reads a cabrillo formatted log and fills a latex template
which can printed and glued into the logbook.

Example usage

```
go run main.go contest.log 14 >/tmp/test.tex && pdflatex /tmp/test.tex
```
